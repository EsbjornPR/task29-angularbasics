# Task 29 - Angular Basics

* Install the Angular CLI (globally)
* Create a new Angular project using the CLI
* Ensure the project can run 
* Remove the unnecessary html from the app.component.html
* Create a string, number, array of strings and Object in the app.component.ts file
* There is no predetermined variables or data, make up your own.
* Display the values from each of these objects in the app.component.html file
* For the array , loop over the array and display all the values in the array
* For the object, if you create and object with three properties i.e. user = { name: 'Dewald', surname: 'Els', age: 'Unknown' } - You must display the values of all three properties with their corresponding name. So the HTML should display name: Dewald, surname: Els etc.


# AngularFEdev

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
