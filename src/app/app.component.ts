import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'angular-FEdev';
  public message: string = 'Things to get a grip on:';
  public year: number = 2020;
  public firstArrOfStr: string[] = [
    'Components',
    'Decorators',
    'NgModules',
    'Dependancy Injection',
    'Services'
  ]
  public typeScriptObject: any = {
    name: 'Johnny',
    surname: 'Cash',
    occupation: 'Artist',
    alive: false,
    born: 1933
  }
}



